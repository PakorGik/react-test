import {User} from '../../Domain/Model/User';

export class LoginSimulationService {
    public login(user: User): void
    {
        localStorage.setItem('user', JSON.stringify(user));
        window.location.href = '/support';
    }

    public logout(): void
    {
        localStorage.removeItem('user');
        window.location.href = '/login';
    }

    public getUser(): User | null
    {
        let userString = localStorage.getItem('user');
        if (userString) {
            let user = JSON.parse(userString);
            return new User(user.login, user.password);
        }
        return null;
    }
}
