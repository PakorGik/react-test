import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Login from './pages/Login';
import Support from './pages/Support';
import './Assets/materialize/css/materialize.min.css';
import Home from './pages/Home';

function AppRouter() {
    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/support" element={<Support />} />
        </Routes>
    );
}

export default AppRouter;
