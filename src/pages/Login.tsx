import React, {FormEvent} from 'react';
import {LoginSimulationService} from '../Application/Service/LoginSimulationService';
import {User} from '../Domain/Model/User';

interface LoginPropsInterface {
}

interface LoginInterface {
    login: string;
    password: string;
}

export class Login extends React.Component<LoginPropsInterface, LoginInterface> {
    constructor(props: LoginPropsInterface, private loginService: LoginSimulationService) {
        super(props);

        this.state = {
            login: '',
            password: '',
        }

        this.loginService = new LoginSimulationService();
    }

    login()
    {
        this.loginService.login(new User(this.state.login, this.state.password));
    }

    submit = (event: FormEvent<HTMLFormElement> | FormEvent<HTMLButtonElement>) =>
    {
        this.login();
        event.preventDefault();
    }

    updateUserState = (e: React.FormEvent<HTMLInputElement>): void => {
        switch (e.currentTarget.id) {
            case 'login':
                this.setState({ login: e.currentTarget.value });
                break;
            case 'password':
                this.setState({ password: e.currentTarget.value });
                break;
        }
    };

    render() {
        return (
            <div className="container">
                <form onSubmit={this.submit} className="row">
                    <h1>Login</h1>
                    <div className="row">
                        <div className="input-field col s12">
                            <input type="text" id="login" placeholder="Login" value={this.state.login} onChange={this.updateUserState} />
                        </div>
                        <div className="input-field col s12">
                            <input type="password" id="password" placeholder="Password" value={this.state.password} onChange={this.updateUserState} />
                        </div>
                        <div className="col s12">
                            <button type='submit' className="waves-effect waves-light btn" onSubmit={this.submit}>Login</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Login;
