import React from 'react';
import {LoginSimulationService} from '../../Application/Service/LoginSimulationService';
import {User} from '../../Domain/Model/User';

interface MenuInterface {
    isLoggedIn: boolean,
    user: User | null,
}

export class Menu extends React.Component<{}, MenuInterface> {
    constructor(props: {}, private loginService: LoginSimulationService) {
        super(props);
        this.loginService = new LoginSimulationService();

        this.state = {
            isLoggedIn: this.loginService.getUser() != null,
            user: this.loginService.getUser()
        }
    }

    logout = () => {
        this.loginService.logout();
        this.setState({ isLoggedIn: false });
    }

    render() {
        if (this.state.isLoggedIn) {
            return (
                <nav>
                    <div className="nav-wrapper">
                        <div className="container">
                            <a href="#" className="brand-logo">React Test</a>
                            <ul id="nav-mobile" className="right hide-on-med-and-down">
                                <li><b>{this.state.user?.login}</b></li>
                                <li><a href="/">Home</a></li>
                                <li><a href="/support">Support</a></li>
                                <li><a onClick={this.logout}>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            );
        }
    }

}

export default Menu;
